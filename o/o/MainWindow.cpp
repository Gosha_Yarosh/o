#include "stdafx.h"
#include "MainWindow.h"
#include "Application.h"
#include <string.h>

//........................Application.............................//

Application App;
BOOL Application::InitInstance()
{
	m_pMainWnd = new MainWindow();
	m_pMainWnd->ShowWindow(SW_RESTORE);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

//........................MainWindow..............................//
MainWindow::MainWindow()
{
	this->Create(NULL, L"HelloWorld!!!", WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, CRect(200, 50, 700, 700));
	this->Processes = new ProcessInformation[100]();
	this->InitInstance();

	this->FillProcessList();
	this->FillProcessList();
	this->FillProcessList();
}
MainWindow::~MainWindow()
{
	delete[] this->Processes;
	this->FreeInstance();
}

void MainWindow::InitInstance()
{
	TimerDelay = TIMER_DELAY_NORMAL;
	SetTimer(1, TimerDelay, NULL);
	
	Font.CreateFont(18, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, 
					CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Calibri");

	this->InitMainMenu();
	CRect Rect;
	this->GetClientRect(Rect);
	Rect.bottom -= 3;
	Rect.left += 2;
	Rect.top += 5;
	TabControl.Create(WS_VISIBLE | WS_CHILD, Rect, this, TAB_CONTROL_ID);
	TabControl.SetFont(&Font, TRUE);
	TabControl.InsertItem(0, L"��������");
	TabControl.InsertItem(1, L"������������������");

	Rect.top += 25;
	Tab1.Create(NULL, NULL, WS_CHILD, Rect, &(this->TabControl), PROCESS_TAB_ID);
	Tab1.InitInstance();
	Tab2.Create(NULL, NULL, WS_CHILD, Rect, &(this->TabControl), PERFOMANCE_TAB_ID);
	Tab2.InitInstance();
	TabControl.SetCurSel(0);
	this->OnTabChange(NULL, NULL);
}
void MainWindow::FreeInstance()
{
	this->Font.DeleteObject();
}
void MainWindow::InitMainMenu()
{
	MainMenu.CreateMenu();

	CMenu* FileSubMenu = new CMenu();
	FileSubMenu->CreatePopupMenu();
	FileSubMenu->AppendMenuW(MF_STRING, ABOUT__MENU_ITEM_ID, L"� ���������");
	FileSubMenu->AppendMenuW(MF_STRING, TERMINATE__MENU_ITEM_ID, L"�����");

	CMenu* ProcessSubMenu = new CMenu();
	ProcessSubMenu->CreatePopupMenu();
	ProcessSubMenu->AppendMenuW(MF_STRING, KILL_PROCESS__MENU_ITEM_ID, L"���������");
	ProcessSubMenu->AppendMenuW(MF_STRING, NEW_PROCESS__MENU_ITEM_ID, L"����� �������");

	CMenu* PrioritySubMenu = new CMenu();
	PrioritySubMenu->CreatePopupMenu();
	PrioritySubMenu->AppendMenuW(MF_STRING, REALTIME_PRIORITY_CLASS, L"��������� �������");
	PrioritySubMenu->AppendMenuW(MF_STRING, HIGH_PRIORITY_CLASS, L"�������");
	PrioritySubMenu->AppendMenuW(MF_STRING, ABOVE_NORMAL_PRIORITY_CLASS, L"���� ��������");
	PrioritySubMenu->AppendMenuW(MF_STRING, NORMAL_PRIORITY_CLASS, L"�������");
	PrioritySubMenu->AppendMenuW(MF_STRING, BELOW_NORMAL_PRIORITY_CLASS, L"���� ��������");
	PrioritySubMenu->AppendMenuW(MF_STRING, IDLE_PRIORITY_CLASS, L"������");
	PrioritySubMenu->CheckMenuItem(NORMAL_PRIORITY_CLASS, 0);
	
	ProcessSubMenu->AppendMenuW(MF_STRING | MF_POPUP, (UINT_PTR)PrioritySubMenu->m_hMenu, L"������ ���������");
	Tab1.PopupMenu = ProcessSubMenu;

	MainMenu.AppendMenuW(MF_STRING | MF_POPUP, (UINT_PTR)FileSubMenu->m_hMenu, L"����");
	MainMenu.AppendMenuW(MF_STRING | MF_POPUP | MF_ENABLED, (UINT_PTR)ProcessSubMenu->m_hMenu, L"�������");
	this->SetMenu(&this->MainMenu);
}
void MainWindow::FillProcessList()
{
	Tab1.ProcessImageList.DeleteImageList();
	Tab1.ProcessImageList.Create(24, 24, ILC_COLOR32, 0, 0);
	Tab1.ProcessList.SetImageList(&(Tab1.ProcessImageList), VIEW_SMALLICONS);
	GetProcessInformation(this->ProcessesCount, this->Processes);
	for (int i = 0; i < this->ProcessesCount; i++)
	{
		WORD IconIndex = 0;
		HICON hIcon;
		if (Processes[i].FileName == L"")
			hIcon = LoadIcon(NULL, MAKEINTRESOURCE(IDI_APPLICATION));
		else
			hIcon = ExtractAssociatedIcon(App.m_hInstance, Processes[i].FileName.GetBuffer(), &(IconIndex));
		Tab1.ProcessImageList.Add(hIcon);
		DestroyIcon(hIcon);
	}
	
	for (int i = 0; i < this->ProcessesCount; i++)
	{
		if (Tab1.ProcessList.GetItemCount() < i + 1)
			Tab1.ProcessList.InsertItem(i, L"", i);
		
		CString IdString;
		IdString.Format(L"%d", this->Processes[i].Id);
		CString KernelLoadString;
		KernelLoadString.Format(L"%.1f%c", this->Processes[i].KernelLoad, '%');

		Tab1.ProcessList.SetItemText(i, 0, this->Processes[i].Name);
		Tab1.ProcessList.SetItemText(i, 1, IdString.GetBuffer());
		Tab1.ProcessList.SetItemText(i, 2, KernelLoadString.GetBuffer());
	}

	while (Tab1.ProcessList.GetItemCount() > ProcessesCount)
		Tab1.ProcessList.DeleteItem(Tab1.ProcessList.GetItemCount() - 1);
}

//........................ProcessTab..............................//
void ProcessTab::InitInstance()
{
	CRect Rect;
	this->GetClientRect(Rect);
	ProcessList.Create(WS_VISIBLE | WS_CHILD | WS_BORDER | LVS_REPORT,
		CRect(Rect.left + 4, Rect.top + 6, Rect.right - 8, Rect.bottom - 10),
		this, 100);

	ProcessList.SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	ProcessList.InsertColumn(0, L"��� ��������", 0, 200);
	ProcessList.InsertColumn(1, L"Id ��������", 0, 100);
	ProcessList.InsertColumn(2, L"�������� ����", 0, 100);
}
void ProcessTab::FreeInstance()
{
	this->ProcessImageList.DeleteImageList();
}

//........................PerfomanceTab...........................//
void PerfomanceTab::InitInstance()
{
	SetTimer(1, TimerDelay, NULL);
}
void PerfomanceTab::FreeInstance()
{

}

//............................RunWindow...........................//
void RunWindow::InitInstance()
{
	Font.CreateFont(18, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Calibri");

	CRect Rect;
	GetClientRect(&Rect);
	this->RunButton.Create(L"���������", WS_VISIBLE | WS_CHILD, 
					       CRect(Rect.right - 100, 10, Rect.right - 5, 45), this, RUN_BUTTON_ID);
	this->RunButton.SetFont(&Font, TRUE);
	this->FileNameEdit.Create(WS_VISIBLE | WS_CHILD | WS_BORDER | WS_TABSTOP, 
							  CRect(5, 15, Rect.right - 5 - 100, 40), this, NULL);
	this->FileNameEdit.SetFont(&Font, TRUE);
}
void RunWindow::FreeInstance()
{

}

//................................................................//
void DrawGraph(CPaintDC& dc, CRect Rect, int MaxXPoints, int MaxYPoints, int* ValuesVector, CString Caption,
			   COLORREF LineColor, COLORREF BorderColor, COLORREF GridColor, COLORREF FillColor)
{
	CPen LinePen(PS_COSMETIC, 1, LineColor);
	CPen BorderPen(PS_COSMETIC, 1, BorderColor);
	CPen GridPen(PS_COSMETIC, 1, GridColor);

	dc.SelectObject(&BorderPen);
	dc.Rectangle(Rect);
	CBrush Back(RGB(255, 255, 255));
	dc.SelectObject(&Back);
	dc.FloodFill(Rect.right - 3, Rect.bottom - 3, BorderColor);
	dc.SelectObject(&LinePen);
	dc.Rectangle(CRect(Rect.left + 1, Rect.top + 1, Rect.right - 1, Rect.bottom - 1));


	CFont Font;
	Font.CreateFont(14, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Calibri");
	dc.SelectObject(Font);
	Font.DeleteObject();
	dc.TextOutW(Rect.right - 26, Rect.top - 15, L"100%");
	dc.TextOutW(Rect.right - 16, Rect.bottom + 1, L"0%");
	dc.TextOutW(Rect.left, Rect.bottom + 1, Caption);

	POINT* Points = new POINT[MaxXPoints]();
	srand(GetTickCount());
	dc.SelectObject(&GridPen);
	for (int i = 0; i < MaxXPoints; i++)
	{
		int dy = (ValuesVector[i] == 0) ? -2 : (-6);
		Points[i].y = dy + Rect.top + (Rect.bottom - Rect.top) - (Rect.bottom - Rect.top) / 100 * ValuesVector[i];
		if (Points[i].y >= Rect.bottom + dy) Points[i].y = Rect.bottom + dy;
		Points[i].x = Rect.left + (Rect.right - Rect.left) * ((double)i / (MaxXPoints - 1));
		

		if (i % 4 == 1)
		{
			dc.MoveTo(Points[i].x, Rect.top + 2);
			dc.LineTo(Points[i].x, Rect.bottom - 2);
		}
	}

	for (int i = 1; i < MaxYPoints; i++)
	{
		int y = Rect.top + (Rect.bottom - Rect.top) * ((double)i / MaxYPoints);
		dc.MoveTo(Rect.left + 2, y);
		dc.LineTo(Rect.right - 2, y);
	}

	Points[0].x = Rect.left + 1;
	Points[MaxXPoints - 1].x = Rect.right - 1;

	dc.SelectObject(&LinePen);
	dc.Polyline(Points, MaxXPoints);


	CBrush FillBrush(FillColor);
	dc.SelectObject(FillBrush);
	dc.FloodFill(Rect.right - 3, Rect.bottom - 3, LineColor);
	FillBrush.DeleteObject();

	delete[] Points;
}

