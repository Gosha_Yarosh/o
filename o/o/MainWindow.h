#pragma once

#include "MainFunctions.h"
#include <afxwin.h>

#define GRAPH_1_LINE_COLOR				RGB(113, 178, 215)
#define GRAPH_2_LINE_COLOR				RGB(149, 40, 180)
#define GRAPH_1_BORDER_COLOR			RGB(17, 125, 187)
#define GRAPH_2_BORDER_COLOR			RGB(139, 18, 174)
#define GRAPH_1_GRID_COLOR				RGB(217, 234, 244)
#define GRAPH_2_GRID_COLOR				RGB(236, 222, 240)
#define GRAPH_1_FILL_COLOR				RGB(241, 246, 250)
#define GRAPH_2_FILL_COLOR				RGB(244, 242, 244)

#define TAB_CONTROL_ID					205
#define PROCESS_TAB_ID					215
#define PERFOMANCE_TAB_ID				225

#define KILL_PROCESS__MENU_ITEM_ID		105
#define NEW_PROCESS__MENU_ITEM_ID		125
#define TERMINATE__MENU_ITEM_ID			135
#define ABOUT__MENU_ITEM_ID				145

#define RUN_BUTTON_ID					315

class ProcessTab : public CWnd
{
public:
	CImageList	ProcessImageList;
	CListCtrl	ProcessList;
	CMenu*		PopupMenu;

	void		InitInstance();
	void		FreeInstance();
protected:
	//afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//DECLARE_MESSAGE_MAP()
};
class PerfomanceTab : public CWnd
{
public:
	CStatic		Graph;

	void		InitInstance();
	void		FreeInstance();
protected:
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT);
	DECLARE_MESSAGE_MAP()
};
class RunWindow : public CFrameWnd
{
public:
	CFont		Font;
	CButton		RunButton;
	CEdit		FileNameEdit;

	void		InitInstance();
	void		FreeInstance();

protected:
	afx_msg void OnRunClick();
	DECLARE_MESSAGE_MAP()
};
class MainWindow : public CFrameWnd
{
public:
	CFont			Font;
	CMenu			MainMenu;
	CTabCtrl		TabControl;
	ProcessTab		Tab1;
	PerfomanceTab	Tab2;
	RunWindow*		RunWnd;

	int						ProcessesCount;
	ProcessInformation*		Processes;
	
	void InitMainMenu();
	void InitInstance();
	void FreeInstance();
	void FillProcessList();

public:
	MainWindow();
	~MainWindow();

protected:
	afx_msg void OnTimer(UINT);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnTabChange(NMHDR* pNMHDR, LRESULT* pResult);

	afx_msg void OnKillProcessClick();
	afx_msg void OnTerminateProgramClick();
	afx_msg void OnStartNewProcessClick();
	afx_msg void OnRestartProcessClick();
	afx_msg BOOL OnPriorityChangeClick(UINT);
	DECLARE_MESSAGE_MAP();
};

void DrawGraph(CPaintDC& dc, CRect Rect, int MaxXPoints,
			   int MaxYPoints, int* ValuesVector, CString Caption,
			   COLORREF LineColor, COLORREF BorderColor, 
			   COLORREF GridColor, COLORREF FillColor);