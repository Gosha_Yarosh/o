#include "stdafx.h"
#include "MainWindow.h"
#include "Application.h"

long long TotalMem, AllocatedMem;
//........................MainWindow..............................//
afx_msg void MainWindow::OnTimer(UINT ID)
{
	for (int i = 1; i < MAX_X_POINTS; i++)
		OldKernelTimeVector[i - 1] = OldKernelTimeVector[i];

	OldKernelTimeVector[MAX_X_POINTS - 1] = 100 - Processes[0].KernelLoad;
	this->FillProcessList();
	GetMemoryInfo(TotalMem, AllocatedMem);
}
afx_msg void MainWindow::OnContextMenu(CWnd* pWnd, CPoint point)
{
	this->Tab1.PopupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
}
afx_msg void MainWindow::OnTabChange(NMHDR* pNMHDR, LRESULT* pResult)
{
	switch (TabControl.GetCurSel())
	{
	case 0:
		Tab1.ShowWindow(TRUE);
		Tab2.ShowWindow(FALSE);
		break;

	case 1:
		Tab1.ShowWindow(FALSE);
		Tab2.ShowWindow(TRUE);
		break;
	}
}
afx_msg void MainWindow::OnKillProcessClick()
{
	int i = Tab1.ProcessList.GetSelectionMark();
	if (i == -1)
		return;

	bool Result = KillProcess(Processes[i].Id);
	if (!Result)
		this->MessageBox(L"�� �� ����� ����� ���. �� ������� ������� :C");
}
afx_msg void MainWindow::OnTerminateProgramClick()
{
	this->CloseWindow();
	delete this;
}
afx_msg void MainWindow::OnStartNewProcessClick()
{
	RunWnd = new RunWindow();
	RunWnd->Create(NULL, L"����� ������", WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU, CRect(350, 300, 750, 400));
	RunWnd->ShowWindow(SW_SHOW);
	RunWnd->InitInstance();
}
afx_msg BOOL MainWindow::OnPriorityChangeClick(UINT Priority)
{

	int i = Tab1.ProcessList.GetSelectionMark();
	if (i == -1)
		return FALSE;

	bool Result = ChangePriority(Processes[i].Id, Priority);
	if (!Result)
		this->MessageBox(L"�� ������� �������� ��������� ��������. �������� ���...");
	return TRUE;
}

BEGIN_MESSAGE_MAP(MainWindow, CFrameWnd)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_CHAR()
	ON_WM_CONTEXTMENU()
	ON_NOTIFY(TCN_SELCHANGE, TAB_CONTROL_ID, OnTabChange)
	ON_COMMAND(KILL_PROCESS__MENU_ITEM_ID, OnKillProcessClick)
	ON_COMMAND(NEW_PROCESS__MENU_ITEM_ID, OnStartNewProcessClick)
	ON_COMMAND(TERMINATE__MENU_ITEM_ID, OnTerminateProgramClick)
	ON_COMMAND_EX(REALTIME_PRIORITY_CLASS, OnPriorityChangeClick)
	ON_COMMAND_EX(HIGH_PRIORITY_CLASS, OnPriorityChangeClick)
	ON_COMMAND_EX(ABOVE_NORMAL_PRIORITY_CLASS, OnPriorityChangeClick)
	ON_COMMAND_EX(NORMAL_PRIORITY_CLASS, OnPriorityChangeClick)
	ON_COMMAND_EX(BELOW_NORMAL_PRIORITY_CLASS, OnPriorityChangeClick)
	ON_COMMAND_EX(IDLE_PRIORITY_CLASS, OnPriorityChangeClick)
END_MESSAGE_MAP()

//........................PerfomanceTab...........................//
afx_msg void PerfomanceTab::OnPaint()
{

	CPaintDC dc(this);
	CRect Rect(10, 80, 460, 230);

	CFont Font;
	Font.CreateFont(40, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Calibri");
	dc.SelectObject(Font);
	Font.DeleteObject();
	dc.TextOutW(10, 4, L"��:");
	dc.TextOutW(10, Rect.bottom + 50, L"������:");

	Font.CreateFont(25, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Calibri");
	dc.SelectObject(Font);
	Font.DeleteObject();
	dc.TextOutW(10, 45, L"�������������:");
	dc.TextOutW(230, 45, L"��������:");
	dc.TextOutW(10, Rect.bottom + 90, L"������������:");
	dc.TextOutW(260, Rect.bottom + 90, L"�����:");

	CString Using;
	Using.Format(L"%d%c", OldKernelTimeVector[MAX_X_POINTS - 1], '%');
	dc.TextOutW(155, 45, L"           ");
	dc.TextOutW(155, 45, Using);

	CString AMem;
	AMem.Format(L"%0.1f ��", (double)AllocatedMem / 1000 / 1000);
	dc.TextOutW(140, Rect.bottom + 90, AMem);

	CString Freq;
	Freq.Format(L"%.1f ���", GetProcessorFrequency());
	dc.TextOutW(320, 45, Freq);

	CString TMem;
	TMem.Format(L"%0.1f ��", (double)TotalMem / 1000 / 1000);
	dc.TextOutW(325, Rect.bottom + 90, TMem);

	DrawGraph(dc, Rect, MAX_X_POINTS, MAX_Y_POINTS, OldKernelTimeVector, L"������������� ���������� %", 
			  GRAPH_1_LINE_COLOR, GRAPH_1_BORDER_COLOR, GRAPH_1_GRID_COLOR, GRAPH_1_FILL_COLOR);

	DrawGraph(dc, CRect(Rect.left, Rect.bottom + 125, Rect.right, Rect.bottom + 125 + 150), MAX_X_POINTS, MAX_Y_POINTS, 
			  OldMemoryCountersVector, L"������������� ������ %",
			  GRAPH_2_LINE_COLOR, GRAPH_2_BORDER_COLOR, GRAPH_2_GRID_COLOR, GRAPH_2_FILL_COLOR);

}
afx_msg void PerfomanceTab::OnTimer(UINT)
{
	this->RedrawWindow();
}

BEGIN_MESSAGE_MAP(PerfomanceTab, CWnd)
	ON_WM_PAINT()
	ON_WM_TIMER()
END_MESSAGE_MAP()

//..........................RunWindow..............................//
afx_msg void RunWindow::OnRunClick()
{
	TCHAR FileName[50]{0};
	int end = this->FileNameEdit.GetLine(0, FileName, 50);
	FileName[end] = NULL;
	USES_CONVERSION;
	int Result = WinExec(T2A(FileName), FALSE);
	if (!(Result > 31))
	{
		CString Msg;
		Msg = L"�� ������� ��������� ����������� ���� ";
		Msg += '"';
		Msg += FileName;
		Msg += '"';
		Msg += L". ���-�� �� ��� �������� :�";
		this->MessageBox(Msg.GetBuffer(), L"������");
	}

	this->CloseWindow();
}

BEGIN_MESSAGE_MAP(RunWindow, CFrameWnd)
	ON_COMMAND(RUN_BUTTON_ID, OnRunClick)
END_MESSAGE_MAP()